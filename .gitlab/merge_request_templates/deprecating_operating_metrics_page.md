## Issue

Closes

Part of: https://gitlab.com/gitlab-data/analytics/issues/1241

Checklist
* [ ]  Move definition off OpMetrics Page `source/handbook/finance/operating-metrics/index.html.md`
* [ ]  Add definition to new place in handbook with `Metric [KPI](/handbook/ceo/kpis/) Definition` format
   * [ ]  Definition includes info on canonical data source and formula, if applicable
   * [ ]  Mentions goal, if applicable
   * [ ]  Links to Periscope dashboard, if applicable
* [ ]  Update link on KPI Mapping page   `source/handbook/business-ops/data-team/metrics/index.html.md`

## Solution

This MR:
