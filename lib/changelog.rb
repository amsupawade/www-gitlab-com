# frozen_string_literal: true

require 'gitlab'
require 'date'

require_relative 'changelog/merge_request'
require_relative 'changelog/file'

module Changelog
  WWW_GITLAB_COM_PROJECT_ID = 7764
end
